import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent{
  title = 'lab6';
  online: boolean;
  addOnline: boolean=true;
  sensors =
    [
      {
        online: this.ranOnline(),
        name:'Датчик 1'
      },
      {
        online: this.ranOnline(),
        name: 'Датчик 2'
      },
      {
        online: this.ranOnline(),
        name: 'Датчик 3'
      },
      {
        online: this.ranOnline(),
        name: 'Датчик 4'
      },
      {
        online: this.ranOnline(),
        name: 'Датчик 5'
      },
      {
        online: this.ranOnline(),
        name: 'Датчик 6'
      }
    ];
  ranOnline(): boolean {
    if (Math.random() >= 0.5) {
      this.online = true;
    }
    else { this.online = false }

    return this.online;
  }
  addSensor() {
    if (this.addOnline === true)
      this.online = true;
    else
      this.online = false;
    let name = "Датчик " + (this.sensors.length+1);
    var a = { online: this.online, name: name };
    this.sensors.push(a);
  }
  deleteSensor() {
    this.sensors.pop();
  }
}
